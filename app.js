if (process.argv.length < 3) {
  console.log('You must select one option between --filter= or --count')
  process.exit(9)
}

const option = process.argv[2]

if (option.startsWith('--filter=')) {
  const { data } = require('./data')
  const { filterCountries } = require('./filter/filter')
  
  const substring = option.split('=')[1]
  const filteredCountries = filterCountries(data, substring)
  
  console.log(JSON.stringify(filteredCountries, null, 2))
  process.exit()
}

if (option === '--count') {
  const { data } = require('./data')
  const { addCount } = require('./count/count')
  
  console.log(JSON.stringify(addCount(data), null, 2))
  process.exit()
}

console.log('Unknown option, only --filter= or --count are available')
process.exit(9)
