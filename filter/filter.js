const filterAnimals = (animals = [], substring) =>
  animals.filter(animal => animal.name.includes(substring))

const filterPeople = (people = [], substring) =>
  people.reduce((acc, currentPerson) => {
    const matchingAnimals = filterAnimals(currentPerson.animals, substring)
    if (!matchingAnimals.length) return acc
    return [
      ...acc,
      {
        ...currentPerson,
        name: currentPerson.name,
        animals: matchingAnimals
      }
    ]
  }, [])

const filterCountries = (countries = [], substring) =>
  countries.reduce((acc, currentCountry) => {
    const matchingPeople = filterPeople(currentCountry.people, substring)
    if (!matchingPeople.length) return acc
    return [
      ...acc,
      {
        ...currentCountry,
        name: currentCountry.name,
        people: matchingPeople
      }
    ]
  }, [])

module.exports = {
  filterAnimals,
  filterPeople,
  filterCountries
}
