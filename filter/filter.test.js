const { filterAnimals, filterPeople, filterCountries } = require('./filter')

describe('filter', () => {
  describe('filterAnimals', () => {
    const animals = [
      { name: 'animal-dog' },
      { name: 'animal-cat' }
    ]
    
    it('should keep animals that have "dog" in its name', () => {
      const expected = [
        { name: 'animal-dog' }
      ]
    
      expect(filterAnimals(animals, 'dog')).toEqual(expected)
    })
  
    it('should return an empty array if no animal has "kangaroo" in its name', () => {
      expect(filterAnimals(animals, 'kangaroo')).toEqual([])
    })
    
    it('should return an empty array if animals is empty', () => {
      expect(filterAnimals([], 'dog')).toEqual([])
    })
    
    it('should return an empty array if no substring is provide', () => {
      expect(filterAnimals(animals)).toEqual([])
    })
  })
  
  describe('filterPeople', () => {
    const people = [
      {
        name: 'person-1',
        animals: [
          { name: 'animal-dog' },
          { name: 'animal-cat' }
        ]
      }, {
        name: 'person-2',
        animals: [
          { name: 'animal-kangaroo' },
          { name: 'animal-giraffe' }
        ]
      }
    ]
    
    it('should keep people with animals that have "dog" in its name', () => {
      const expected = [
        {
          name: 'person-1',
          animals: [
            { name: 'animal-dog' }
          ]
        }
      ]
    
      expect(filterPeople(people, 'dog')).toEqual(expected)
    })
  
    it('should return an empty array if nobody owns an animal that have "pika" in its name', () => {
      expect(filterPeople(people, 'pika')).toEqual([])
    })
  
    it('should return an empty array if people is empty', () => {
      expect(filterPeople([], 'dog')).toEqual([])
    })
  
    it('should return an empty array if no substring is provide', () => {
      expect(filterPeople(people)).toEqual([])
    })
  })
  
  describe('filterCountries', () => {
    const countries = [
      {
        name: 'country-1',
        people: [
          {
            name: 'person-1-of-country-1',
            animals: [
              { name: 'animal-dog' },
              { name: 'animal-cat' }
            ]
          }, {
            name: 'person-2-of-country-1',
            animals: [
              { name: 'animal-kangaroo' },
              { name: 'animal-giraffe' }
            ]
          }
        ]
      }, {
        name: 'country-2',
        people: [
          {
            name: 'person-1-of-country-2',
            animals: [
              { name: 'animal-fox' },
              { name: 'animal-lion' }
            ]
          }, {
            name: 'person-2-of-country-2',
            animals: [
              { name: 'animal-squirrel' },
              { name: 'animal-panda' }
            ]
          }
        ]
      }
    ]
    
    it('should keep countries with people with animals that have "kangaroo" in its name', () => {
      const expected = [
        {
          name: 'country-1',
          people: [
            {
              name: 'person-2-of-country-1',
              animals: [
                { name: 'animal-kangaroo' }
              ]
            }
          ]
        }
      ]
      
      expect(filterCountries(countries, 'kangaroo')).toEqual(expected)
    })
    
    it('should return an empty array if no country contains people with animals that have "blobfish" in its name', () => {
      expect(filterCountries(countries, 'blobfish')).toEqual([])
    })
  
    it('should return an empty array if countries is empty', () => {
      expect(filterCountries([], 'dog')).toEqual([])
    })
  
    it('should return an empty array if no substring is provide', () => {
      expect(filterCountries(countries)).toEqual([])
    })
  })
})
