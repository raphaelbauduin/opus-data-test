const addAnimalsCountInPerson = person => {
  if (!person) return
  return {
    ...person,
    name: `${ person.name } [${ person.animals && person.animals.length || 0 }]`
  }
}

const addPeopleCountInCountry = country => {
  if (!country) return
  return {
    ...country,
    name: `${ country.name } [${ country.people && country.people.length || 0 }]`,
    ...(country.people && { people: country.people.map(person => addAnimalsCountInPerson(person)) })
  }
}

const addCount = (countries = []) => {
  return countries.map(country => addPeopleCountInCountry(country))
}


module.exports = {
  addAnimalsCountInPerson,
  addPeopleCountInCountry,
  addCount
}
