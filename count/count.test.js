const { addAnimalsCountInPerson, addPeopleCountInCountry, addCount } = require('./count')

describe('count', () => {
  describe('addAnimalsInPersonCount', () => {
    it('should add the number of animals in person name', () => {
      const person = {
        name: 'person-1',
        animals: [
          { name: 'animal-dog' },
          { name: 'animal-cat' }
        ]
      }
      
      const expected = {
        name: 'person-1 [2]',
        animals: [
          { name: 'animal-dog' },
          { name: 'animal-cat' }
        ]
      }
      
      expect(addAnimalsCountInPerson(person)).toEqual(expected)
    })
  
    it('should add "0" if no animals is provide', () => {
      const person = {
        name: 'person-1',
      }
      const expected = {
        name: 'person-1 [0]'
      }
      expect(addAnimalsCountInPerson(person)).toEqual(expected)
    })
  
    it('should be undefined if no person is provide', () => {
      expect(addAnimalsCountInPerson()).toBeUndefined()
    })
  })
  
  describe('addPeopleCountInCountry', () => {
    it('should add the number of people in country name and the number of animals in person name', () => {
      const country = {
        name: 'country-1',
        people: [
          {
            name: 'person-1-of-country-1',
            animals: [
              { name: 'animal-dog' }
            ]
          }, {
            name: 'person-2-of-country-1',
            animals: [
              { name: 'animal-cat' },
              { name: 'animal-kangaroo' },
              { name: 'animal-giraffe' }
            ]
          }
        ]
      }
  
      const expected = {
        name: 'country-1 [2]',
        people: [
          {
            name: 'person-1-of-country-1 [1]',
            animals: [
              { name: 'animal-dog' }
            ]
          }, {
            name: 'person-2-of-country-1 [3]',
            animals: [
              { name: 'animal-cat' },
              { name: 'animal-kangaroo' },
              { name: 'animal-giraffe' }
            ]
          }
        ]
      }
    
      expect(addPeopleCountInCountry(country)).toEqual(expected)
    })
  
    it('should add "0" if no people is provide', () => {
      const country = {
        name: 'country-1'
      }
      
      const expected = {
        name: 'country-1 [0]'
      }
      
      expect(addPeopleCountInCountry(country)).toEqual(expected)
    })
  
    it('should be undefined if no country is provide', () => {
      expect(addPeopleCountInCountry()).toBeUndefined()
    })
  })
  
  describe('addCount', () => {
  
    it('should add the number of people in country name and the number of animals in person name for countries', () => {
      const countries = [
        {
          name: 'country-1',
          people: [
            {
              name: 'person-1-of-country-1',
              animals: [
                { name: 'animal-dog' }
              ]
            }, {
              name: 'person-2-of-country-1',
              animals: [
                { name: 'animal-cat' },
                { name: 'animal-kangaroo' },
                { name: 'animal-giraffe' }
              ]
            }
          ]
        }, {
          name: 'country-2',
          people: [
            {
              name: 'person-1-of-country-2',
              animals: [
                { name: 'animal-fox' },
                { name: 'animal-lion' }
              ]
            }
          ]
        }
      ]
      const expected = [
        {
          name: 'country-1 [2]',
          people: [
            {
              name: 'person-1-of-country-1 [1]',
              animals: [
                { name: 'animal-dog' }
              ]
            }, {
              name: 'person-2-of-country-1 [3]',
              animals: [
                { name: 'animal-cat' },
                { name: 'animal-kangaroo' },
                { name: 'animal-giraffe' }
              ]
            }
          ]
        }, {
          name: 'country-2 [1]',
          people: [
            {
              name: 'person-1-of-country-2 [2]',
              animals: [
                { name: 'animal-fox' },
                { name: 'animal-lion' }
              ]
            }
          ]
        }
      ]
      expect(addCount(countries)).toEqual(expected)
    })
    
    it('should return an empty array if no countries is provide', () => {
      expect(addCount()).toEqual([])
    })
  })
})
